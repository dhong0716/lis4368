> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# Advanced Web Applications Development using Java and JSP

## Dong Min Hong

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Git commands with short descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Created personal MySQL account
    - Connected MySQL database to java programming
    - Created querybooks
    - Published personal website 

 3. [A3 README.md](a3/README.md)
    - Create Pets, Customer and Petstore tables
    - MySQL Workbench ERD
    - Forward Engineer 

 4. [P1 README.md](p1/README.md)
    - Create a link using java and JSP(JavaServer Pages)
    - Create fill in boxes for customer
    - Warn the user when there is no input

 5. [A4 README.md](a4/README.md)
    - Work Server-Side validation
    - Data that are not input gets failed validation
    - Create thank you page if validation is passed

 6. [A5 README.md](a5/README.md)
    - Work Server-Side validation
    - Data that are not input gets failed validation
    - Create thank you page if validation is passed
    - Forward data entered to the database.

 7. [P2 README.md](p2/README.md)
	- Inserting data into local database
	- Preventing XSS and SQL Injection
	- Basic server side validation

