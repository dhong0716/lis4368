> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4364

## Dong Min Hong

### Assignment 3 # Requirements:

*Sub-Heading:*

1. MySQL Workbench ERD
2. Forward Engineer 





#### Assignment Screenshots:

*Screenshot of ERD:

![MySQL ERD Image](img/a3.png)

*Screenshot MyEvent Homepage*:

#### Tutorial Links:

*A3.mwb ERD:*
[A3.mwb](docs/a3.mwb)

*A3.sql commandlines:*
[A3.sql](docs/a3.sql)
