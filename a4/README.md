> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4364

## Dong Min Hong

### Assignment 4 # Requirements:

*Sub-Heading:*

1. Work Server-Side validation
2. Data that are not input gets failed validation
3. Create thank you page if validation is passed





#### Assignment Screenshots:

*Screenshot of Failed Validation:

![Failed Validation](image/a41.png)

*Screenshot of Passed Validation*:

![Passed Validation](image/a42.png)


