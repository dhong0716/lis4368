> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4364

## Dong Min Hong

### Project 1 # Requirements:

*Sub-Heading:*

1. Create a link
2. Create fill in boxes for customer
3. Warn the user when there is no input





#### Assignment Screenshots:

*Requirments to fill in the boxes:

![Requires input in boxes](img/p1.png)


*Requirements are met and all the boxes are filled correctly:

![Required are met](img/p12.png)

*index.jsp for p1:*
[p1 index.jsp](index.jsp)

*index.jsp for default page:*
[index.jsp](https://bitbucket.org/dhong0716/lis4368/src/2b15ab845a8d0a6188f5e6ba635a85fbcaa942e6/index.jsp?at=master&fileviewer=file-view-default)



