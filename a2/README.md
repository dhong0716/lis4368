> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Dong Min Hong

### LIS4368 Requirements:

*Sub-Heading:*

1. Created personal MySQL account
2. Connected MySQL database to java programming
3. Created querybooks
4. Published personal website 


LIS4368 Assignment Screenshots:


*http://localhost:9999/hello*:

![http://localhost:9999/hello](img/1.png)

*http://localhost:9999/hello/index.html*:

![http://localhost:9999/hello/index.html](img/2.png)

*http://localhost:9999/hello/sayhello*:

![http://localhost:9999/hello/sayhello](img/3.png)

*http://localhost:9999/hello/querybook.html*:

![http://localhost:9999/hello/querybook.html](img/4.png)

*http://localhost:9999/hello/sayhi*:

![http://localhost:9999/hello/sayhi](img/5.png)

*http://localhost:9999/hello/querybook.html*:

![http://localhost:9999/hello/querybook.html](img/6.png)

*query results*:

![query results](img/7.png)



