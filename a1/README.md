> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Dong Min Hong

### LIS4368 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installation
3. Chapter Questions

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of Tomcat
* git commands w/short description
* Bitbuket repo links: a)this assignment and b)the completed tutorials above (bitbucketlocations and myteamquotes).



> #### Git commands w/short descriptions:

1. git init - make an exisiting directory of content into a new Git repository.
2. git status - see anything has been modified since last commit.
3. git add - includes changes that was made in next commit.
4. git commit - gives output of git status.
5. git push -  shows which branch is pushed.
6. git pull - bring a local branch up-to-date with remote version.
7. git diff - shows what the changes are line by line.

#### Assignment Screenshots:


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/a12.png)

*Screenshot of running http://localhost:9999

![Tomcat Installation Screenshot](img/a1.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/dhong0716/bitbucketstationlocations/src/428bd1eaa1733ced540140af9fd3670d75af3132/stationlocations?at=master&fileviewer=file-view-default "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/dhong0716/myteamquotes/overview "My Team Quotes Tutorial")
